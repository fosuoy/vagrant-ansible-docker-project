from testinfra.utils.ansible_runner import AnsibleRunner

testinfra_hosts = AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('vagrant-docker-host')


def test_docker_is_installed(host):
    docker = host.package("docker")
    assert docker.is_installed


def test_docker_service_running_and_enabled(host):
    docker = host.service("docker")
    assert docker.is_running
    assert docker.is_enabled


def test_docker_socket_is_listening(host):
    docker_socket = host.socket("unix:///var/run/docker.sock")
    assert docker_socket.is_listening


def test_if_docker_container_process_is_present(host):
    assert host.process.get(user="root", comm="dockerd-current")


def test_if_nginx_process_is_running_under_docker(host):
    assert host.process.get(user="root", comm="nginx")


def test_if_content_is_being_served_on_port_80(host):
    curl_localhost = host.run("curl localhost")
    assert curl_localhost.stdout == "Hello World"


def test_that_port80_is_not_listening_on_the_host(host):
    port_80 = host.socket("tcp://127.0.0.1:80")
    assert not port_80.is_listening
