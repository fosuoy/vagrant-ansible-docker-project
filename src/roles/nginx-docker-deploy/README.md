Nginx Docker Deploy
=========
A role to create a test nginx container which contains a hello world application
and expose it systemwide

Role Variables
--------------

DockerBaseDirectory    => where to place docker file and related
DockerBaseImage        => what the base image of the nginx container should be
DockerContainerName    => what to name the container
DockerContainerVersion => what version to tag the container
HelloWorldNginxMessage => the Hello World text to be served by Nginx

default values are present in ./defaults/main.yml

Dependencies
------------
install-docker role which is packaged here also

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: nginx-docker-deploy,
             DockerBaseImage: "ubuntu/xenial64",
             HelloWorldNginxMessage: "Testing if this works" }
