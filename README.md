# Vagrant Docker test


This is a test ansible project which configures a Vagrant host.


It installs Docker and configures a docker container running Nginx based on the
latest Alpine image, it then deploys this container and makes it available to
the base host at address `192.168.50.4`.


## Requirements
To run it you must have vagrant installed either from
your package manager or here:
https://www.vagrantup.com/

V1.9.7 is recommended


Some dependencies are required from the pip package manager, first install pip,
available as python-pip in most distributions, then create a virtual
environment:

`virtualenv env`

`source env/bin/activate`


Then to install just:
`pip install -r requirements.txt`


## What to do

After cloning this repository you should be able to run:

`vagrant up`

within the root directory of the project, this should start up a base CentOS/7
box and configure it with docker + nginx container with a hello world page which
should be accessible from the host OS (that is running Vagrant) at IP:

`192.168.50.4`


## How to Test

Enter the src directory and run:

`molecule test`

to test that the Ansible roles are running correctly

The tests are written using testinfra and are available within the
src/tests/test_default.py file
